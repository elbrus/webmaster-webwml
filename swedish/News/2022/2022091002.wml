<define-tag pagetitle>Uppdaterad Debian 11; 11.5 utgiven</define-tag>
<define-tag release_date>2022-09-10</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="4924e09e5cb1b4163d7ec7161488354e4167b24c"

<define-tag release>11</define-tag>
<define-tag codename>bullseye</define-tag>
<define-tag revision>11.5</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Debianprojektet presenterar stolt sin femte uppdatering till dess
stabila utgåva Debian <release> (med kodnamnet <q><codename></q>). 
Denna punktutgåva lägger huvudsakligen till rättningar för säkerhetsproblem,
tillsammans med ytterligare rättningar för allvarliga problem. Säkerhetsbulletiner
har redan publicerats separat och refereras när de finns tillgängliga.</p>

<p>Vänligen notera att punktutgåvan inte innebär en ny version av Debian 
<release> utan endast uppdaterar några av de inkluderade paketen. Det behövs
inte kastas bort gamla media av <q><codename></q>. Efter installationen
kan paket uppgraderas till de aktuella versionerna genom att använda en uppdaterad
Debianspegling..</p>

<p>De som frekvent installerar uppdateringar från security.debian.org kommer inte att behöva
uppdatera många paket, och de flesta av sådana uppdateringar finns
inkluderade i punktutgåvan.</p>

<p>Nya installationsavbildningar kommer snart att finnas tillgängliga på de vanliga platserna.</p>

<p>En uppgradering av en existerande installation till denna revision kan utföras genom att
peka pakethanteringssystemet på en av Debians många HTTP-speglingar.
En utförlig lista på speglingar finns på:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Blandade felrättningar</h2>

<p>Denna uppdatering av den stabila utgåvan lägger till några viktiga felrättningar till följande paket:</p>

<table border=0>
<tr><th>Paket</th>               <th>Orsak</th></tr>
<correction avahi "Fix display of URLs containing '&amp;' in avahi-discover; do not disable timeout cleanup on watch cleanup; fix NULL pointer crashes when trying to resolve badly-formatted hostnames [CVE-2021-3502]">
<correction base-files "Update /etc/debian_version for the 11.5 point release">
<correction cargo-mozilla "New source package to support building of newer firefox-esr and thunderbird versions">
<correction clamav "New upstream stable release">
<correction commons-daemon "Fix JVM detection">
<correction curl "Reject cookies with <q>control bytes</q> [CVE-2022-35252]">
<correction dbus-broker "Fix assertion failure when disconnecting peer groups; fix memory leak; fix null pointer dereference [CVE-2022-31213]">
<correction debian-installer "Rebuild against proposed-updates; increase Linux kernel ABI to 5.10.0-18">
<correction debian-installer-netboot-images "Rebuild against proposed-updates; increase Linux kernel ABI to 5.10.0-18">
<correction debian-security-support "Update support status of various packages">
<correction debootstrap "Ensure non-merged-usr chroots can continue to be created for older releases and buildd chroots">
<correction dlt-daemon "Fix double free issue [CVE-2022-31291]">
<correction dnsproxy "Listen on localhost by default, rather than the possibly unavailable 192.168.168.1">
<correction dovecot "Fix possible security issues when two passdb configuration entries exist with the same driver and args settings [CVE-2022-30550]">
<correction dpkg "Fix conffile removal-on-upgrade handling, memory leak in remove-on-upgrade handling; Dpkg::Shlibs::Objdump: Fix apply_relocations to work with versioned symbols; add support for ARCv2 CPU; several updates and fixes to dpkg-fsys-usrunmess">
<correction fig2dev "Fix double free issue [CVE-2021-37529], denial of service issue [CVE-2021-37530]; stop misplacement of embedded eps images">
<correction foxtrotgps "Fix crash by ensuring that threads are always unreferenced">
<correction gif2apng "Fix heap-based buffer overflows [CVE-2021-45909 CVE-2021-45910 CVE-2021-45911]">
<correction glibc "Fix an off-by-one buffer overflow/underflow in getcwd() [CVE-2021-3999]; fix several overflows in wide character functions; add a few EVEX optimized string functions to fix a performance issue (up to 40%) with Skylake-X processors; make grantpt usable after multi-threaded fork; ensure that libio vtable protection is enabled">
<correction golang-github-pkg-term "Fix building on newer Linux kernels">
<correction gri "Use <q>ps2pdf</q> instead of <q>convert</q> for converting from PS to PDF">
<correction grub-efi-amd64-signed "New upstream release">
<correction grub-efi-arm64-signed "New upstream release">
<correction grub-efi-ia32-signed "New upstream release">
<correction grub2 "New upstream release">
<correction http-parser "Unset F_CHUNKED on new Transfer-Encoding, fixing possible HTTP request smuggling issue [CVE-2020-8287]">
<correction ifenslave "Fix bonded interface configurations">
<correction inetutils "Fix buffer overflow issue [CVE-2019-0053], stack exhaustion issue, handling of FTP PASV responses [CVE-2021-40491], denial of service issue [CVE-2022-39028]">
<correction knot "Fix IXFR to AXFR fallback with dnsmasq">
<correction krb5 "Use SHA256 as Pkinit CMS Digest">
<correction libayatana-appindicator "Provide compatibility for software that depends on libappindicator">
<correction libdatetime-timezone-perl "Update included data">
<correction libhttp-daemon-perl "Improve handling of Content-Length header [CVE-2022-31081]">
<correction libreoffice "Support EUR in .hr locale; add HRK&lt;-&gt;EUR conversion rate to Calc and the Euro Wizard; security fixes [CVE-2021-25636 CVE-2022-26305 CVE-2022-26306 CVE-2022-26307]; fix hang accessing Evolution address books">
<correction linux "New upstream stable release">
<correction linux-signed-amd64 "New upstream stable release">
<correction linux-signed-arm64 "New upstream stable release">
<correction linux-signed-i386 "New upstream stable release">
<correction llvm-toolchain-13 "New source package to support building of newer firefox-esr and thunderbird versions">
<correction lwip "Fix buffer overflow issues [CVE-2020-22283 CVE-2020-22284]">
<correction mokutil "New upstream version, to allow for SBAT management">
<correction node-log4js "Do not create world-readable files by default [CVE-2022-21704]">
<correction node-moment "Fix regular expression-based denial of service issue [CVE-2022-31129]">
<correction nvidia-graphics-drivers "New upstream release; security fixes [CVE-2022-31607 CVE-2022-31608 CVE-2022-31615]">
<correction nvidia-graphics-drivers-legacy-390xx "New upstream release; security fixes [CVE-2022-31607 CVE-2022-31608 CVE-2022-31615]">
<correction nvidia-graphics-drivers-tesla-450 "New upstream release; security fixes [CVE-2022-31607 CVE-2022-31608 CVE-2022-31615]">
<correction nvidia-graphics-drivers-tesla-470 "New upstream release; security fixes [CVE-2022-31607 CVE-2022-31608 CVE-2022-31615]">
<correction nvidia-settings "New upstream release; fix cross-building">
<correction nvidia-settings-tesla-470 "New upstream release; fix cross-building">
<correction pcre2 "Fix out-of-bounds read issues [CVE-2022-1586 CVE-2022-1587]">
<correction postgresql-13 "Do not let extension scripts replace objects not already belonging to the extension [CVE-2022-2625]">
<correction publicsuffix "Update included data">
<correction rocksdb "Fix illegal instruction on arm64">
<correction sbuild "Buildd::Mail: support MIME encoded Subject: header, also copy the Content-Type: header when forwarding mail">
<correction systemd "Drop bundled copy of linux/if_arp.h, fixing build failures with newer kernel headers; support detection for ARM64 Hyper-V guests; detect OpenStack instance as KVM on arm">
<correction twitter-bootstrap4 "Actually install CSS map files">
<correction tzdata "Update timezone data for Iran and Chile">
<correction xtables-addons "Support both old and new versions of security_skb_classify_flow()">
</table>


<h2>Säkerhetsuppdateringar</h2>


<p>Denna revision lägger till följande säkerhetsuppdateringar till den stabila utgåvan.
Säkerhetsgruppen har redan släppt bulletiner för alla dessa
uppdateringar:</p>

<table border=0>
<tr><th>Bulletin-ID</th>  <th>Paket</th></tr>
<dsa 2022 5175 thunderbird>
<dsa 2022 5176 blender>
<dsa 2022 5177 ldap-account-manager>
<dsa 2022 5178 intel-microcode>
<dsa 2022 5179 php7.4>
<dsa 2022 5180 chromium>
<dsa 2022 5181 request-tracker4>
<dsa 2022 5182 webkit2gtk>
<dsa 2022 5183 wpewebkit>
<dsa 2022 5184 xen>
<dsa 2022 5185 mat2>
<dsa 2022 5187 chromium>
<dsa 2022 5188 openjdk-11>
<dsa 2022 5189 gsasl>
<dsa 2022 5190 spip>
<dsa 2022 5191 linux-signed-amd64>
<dsa 2022 5191 linux-signed-arm64>
<dsa 2022 5191 linux-signed-i386>
<dsa 2022 5191 linux>
<dsa 2022 5192 openjdk-17>
<dsa 2022 5193 firefox-esr>
<dsa 2022 5194 booth>
<dsa 2022 5195 thunderbird>
<dsa 2022 5196 libpgjava>
<dsa 2022 5197 curl>
<dsa 2022 5198 jetty9>
<dsa 2022 5199 xorg-server>
<dsa 2022 5200 libtirpc>
<dsa 2022 5201 chromium>
<dsa 2022 5202 unzip>
<dsa 2022 5203 gnutls28>
<dsa 2022 5204 gst-plugins-good1.0>
<dsa 2022 5205 ldb>
<dsa 2022 5205 samba>
<dsa 2022 5206 trafficserver>
<dsa 2022 5207 linux-signed-amd64>
<dsa 2022 5207 linux-signed-arm64>
<dsa 2022 5207 linux-signed-i386>
<dsa 2022 5207 linux>
<dsa 2022 5208 epiphany-browser>
<dsa 2022 5209 net-snmp>
<dsa 2022 5210 webkit2gtk>
<dsa 2022 5211 wpewebkit>
<dsa 2022 5213 schroot>
<dsa 2022 5214 kicad>
<dsa 2022 5215 open-vm-tools>
<dsa 2022 5216 libxslt>
<dsa 2022 5217 firefox-esr>
<dsa 2022 5218 zlib>
<dsa 2022 5219 webkit2gtk>
<dsa 2022 5220 wpewebkit>
<dsa 2022 5221 thunderbird>
<dsa 2022 5222 dpdk>
</table>


<h2>Borttagna paket</h2>

<p>Följande paket har tagits bort på grund av omständigheter utom vår kontroll:</p>

<table border=0>
<tr><th>Paket</th>               <th>Orsak</th></tr>
<correction evenement "Icke underhållen; behövdes endast för redan borttagen movim">
<correction php-cocur-slugify "Icke underhållen; behövdes endast för redan borttagen movim">
<correction php-defuse-php-encryption "Icke underhållen; behövdes endast för redan borttagen movim">
<correction php-dflydev-fig-cookies "Icke underhållen; behövdes endast för redan borttagen movim">
<correction php-embed "Icke underhållen; behövdes endast för redan borttagen movim">
<correction php-fabiang-sasl "Icke underhållen; behövdes endast för redan borttagen movim">
<correction php-markdown "Icke underhållen; behövdes endast för redan borttagen movim">
<correction php-raintpl "Icke underhållen; behövdes endast för redan borttagen movim">
<correction php-react-child-process "Icke underhållen; behövdes endast för redan borttagen movim">
<correction php-react-http "Icke underhållen; behövdes endast för redan borttagen movim">
<correction php-respect-validation "Icke underhållen; behövdes endast för redan borttagen movim">
<correction php-robmorgan-phinx "Icke underhållen; behövdes endast för redan borttagen movim">
<correction ratchet-pawl "Icke underhållen; behövdes endast för redan borttagen movim">
<correction ratchet-rfc6455 "Icke underhållen; behövdes endast för redan borttagen movim">
<correction ratchetphp "Icke underhållen; behövdes endast för redan borttagen movim">
<correction reactphp-cache "Icke underhållen; behövdes endast för redan borttagen movim">
<correction reactphp-dns "Icke underhållen; behövdes endast för redan borttagen movim">
<correction reactphp-event-loop "Icke underhållen; behövdes endast för redan borttagen movim">
<correction reactphp-promise-stream "Icke underhållen; behövdes endast för redan borttagen movim">
<correction reactphp-promise-timer "Icke underhållen; behövdes endast för redan borttagen movim">
<correction reactphp-socket "Icke underhållen; behövdes endast för redan borttagen movim">
<correction reactphp-stream "Icke underhållen; behövdes endast för redan borttagen movim">

</table>

<h2>Debianinstalleraren</h2>
<p>Installeraren har uppdaterats för att inkludera rättningarna som har inkluderats i den
stabila utgåvan med denna punktutgåva.</p>

<h2>URLer</h2>

<p>Den fullständiga listan på paket som har förändrats i denna revision:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Den aktuella stabila utgåvan:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Föreslagna uppdateringar till den stabila utgåvan:</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>Information om den stabila utgåvan (versionsfakta, kända problem osv.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Säkerhetsbulletiner och information:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Om Debian</h2>

<p>Debianprojektet är en grupp utvecklare av Fri mjukvara som
donerar sin tid och kraft för att producera det helt
fria operativsystemet Debian.</p>

<h2>Kontaktinformation</h2>

<p>För ytterligare information, vänligen besök Debians webbplats på
<a href="$(HOME)/">https://www.debian.org/</a>, skicka e-post till
&lt;press@debian.org&gt;, eller kontakta gruppen för stabila utgåvor på
&lt;debian-release@lists.debian.org&gt;.</p>


