#use wml::debian::translation-check translation="cb6bc3d73ebe6c8ee975e84a5de151bbec375689" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes ont été découverts dans le serveur de base de données
MySQL. Les vulnérabilités sont corrigées en mettant à niveau MySQL vers la
nouvelle version 5.5.58 de l’amont qui inclut des modifications supplémentaires,
telles que des améliorations de performance, des corrections de bogue, de
nouvelles fonctionnalités et éventuellement des changements incompatibles.
Veuillez consulter les notes de publication de MySQL 5.5 et les annonces de
mises à jour critiques d’Oracle pour de plus amples détails :</p>

<ul>
<li><url "https://dev.mysql.com/doc/relnotes/mysql/5.5/en/news-5-5-58.html"></li>
<li><a href="http://www.oracle.com/technetwork/security-advisory/cpuoct2017-3236626.html">http://www.oracle.com/technetwork/security-advisory/cpuoct2017-3236626.html</a></li>
</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 5.5.58-0+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets mysql-5.5.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1141.data"
# $Id: $
