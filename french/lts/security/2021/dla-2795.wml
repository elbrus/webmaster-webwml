#use wml::debian::translation-check translation="6c9ea0ea3dc5caf6568a3f25d73cef7d01e0fb61" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité de sécurité a été découverte dans gpsd, le démon de
géo-positionnement par satellite. Un dépassement de pile pourrait permettre
à des attaquants distants l'exécution de code arbitraire à l’aide du trafic
sur le port 2947/TCP ou d’entrées JSON contrefaites.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
3.16-4+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets gpsd.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de gpsd, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/gpsd">\
https://security-tracker.debian.org/tracker/gpsd</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
\<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2795.data"
# $Id: $
