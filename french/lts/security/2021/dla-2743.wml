#use wml::debian::translation-check translation="a93dbe0bb8e753e35b938246154f9a7af56d4bfd" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il a été découvert que les systèmes avec des microprocesseurs utilisant
l’exécution spéculative et la prédiction de branchement indirect peuvent
permettre une divulgation non autorisée d’information à un attaquant avec
un accès d’utilisateur local à l'aide d'une analyse de canal auxiliaire
(Spectre version 2).
Plusieurs correctifs ont été déjà apportés dans le noyau Linux, le microcode
pour les CPU d’Intel et al. Ce correctif ajoute la prise en charge d’IBPB basée
sur le microcode pour les CPU d’AMD.</p>

<p>Pour Debian 9 « Stretch », ce problème a été corrigé dans
la version 3.20181128.1~deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets amd64-microcode.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de amd64-microcode,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/amd64-microcode">\
https://security-tracker.debian.org/tracker/amd64-microcode</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2743.data"
# $Id: $
