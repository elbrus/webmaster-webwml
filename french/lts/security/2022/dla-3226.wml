#use wml::debian::translation-check translation="a3bdff221e2f47b4855537334c4da1f0726bdb77" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Lors de l’analyse de fichiers contenant des données de polygone Nef,
plusieurs violations d’accès en mémoire peuvent se produire, la plupart pouvant
permettre une exécution de code.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28601">CVE-2020-28601</a>

<p>Une vulnérabilité d’exécution de code existe dans la fonction
Nef d’analyse de polygone de CGAL. Une vulnérabilité de lecture hors limites existe
dans <code>Nef_2/PM_io_parser.h</code> <code>PM_io_parser::read_vertex()</code>
<code>Face_of[]</code>. Un attaquant peut fournir une entrée malveillante
pour déclencher cette vulnérabilité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28602">CVE-2020-28602</a>

<p>Plusieurs vulnérabilités d’exécution de code existent dans la fonction Nef
d’analyse de polygone de CGAL. Un fichier spécialement contrefait peut conduire à
une lecture hors limites et une confusion de type, ce qui pourrait conduire à
une exécution de code. Un attaquant peut fournir une entrée malveillante pour
déclencher n’importe quelle de ces vulnérabilités. Une vulnérabilité de lecture
hors limites existe dans <code>Nef_2/PM_io_parser.h</code>
<code>PM_io_parser&lt;PMDEC&gt;::read_vertex()</code>
<code>Halfedge_of[]</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28603">CVE-2020-28603</a>

<p>Plusieurs vulnérabilités d’exécution de code existent dans la fonction Nef
d’analyse de polygone de CGAL. Un fichier spécialement contrefait peut conduire à
une lecture hors limites et une confusion de type, ce qui pourrait conduire à
une exécution de code. Un attaquant peut fournir une entrée malveillante pour
déclencher n’importe quelle de ces vulnérabilités. Une vulnérabilité de lecture
hors limites existe dans <code>Nef_2/PM_io_parser.h</code>
<code>PM_io_parser&lt;PMDEC&gt;::read_hedge()</code>
<code>e-&gt;set_prev()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28604">CVE-2020-28604</a>

<p>Plusieurs vulnérabilités d’exécution de code existent dans la fonction Nef
d’analyse de polygone de CGAL. Un fichier spécialement contrefait peut conduire à
une lecture hors limites et une confusion de type, ce qui pourrait conduire à
une exécution de code. Un attaquant peut fournir une entrée malveillante pour
déclencher n’importe quelle de ces vulnérabilités. Une vulnérabilité de lecture
hors limites existe dans <code>Nef_2/PM_io_parser.h</code>
<code>PM_io_parser&lt;PMDEC&gt;::read_hedge()</code>
<code>e-&gt;set_next()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28605">CVE-2020-28605</a>

<p>Plusieurs vulnérabilités d’exécution de code existent dans la fonction Nef
d’analyse de polygone de CGAL. Un fichier spécialement contrefait peut conduire à
une lecture hors limites et une confusion de type, ce qui pourrait conduire à
une exécution de code. Un attaquant peut fournir une entrée malveillante pour
déclencher n’importe quelle de ces vulnérabilités. Une vulnérabilité de lecture
hors limites existe dans <code>Nef_2/PM_io_parser.h</code>
<code>PM_io_parser&lt;PMDEC&gt;::read_hedge()</code>
<code>e-&gt;set_vertex()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28606">CVE-2020-28606</a>

<p>Plusieurs vulnérabilités d’exécution de code existent dans la fonction Nef
d’analyse de polygone de CGAL. Un fichier spécialement contrefait peut conduire à
une lecture hors limites et une confusion de type, ce qui pourrait conduire à
une exécution de code. Un attaquant peut fournir une entrée malveillante pour
déclencher n’importe quelle de ces vulnérabilités. Une vulnérabilité de lecture
hors limites existe dans <code>Nef_2/PM_io_parser.h</code>
<code>PM_io_parser&lt;PMDEC&gt;::read_hedge()</code>
<code>e-&gt;set_face()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28607">CVE-2020-28607</a>

<p>Plusieurs vulnérabilités d’exécution de code existent dans la fonction Nef
d’analyse de polygone de CGAL. Un fichier spécialement contrefait peut conduire à
une lecture hors limites et une confusion de type, ce qui pourrait conduire à
une exécution de code. Un attaquant peut fournir une entrée malveillante pour
déclencher n’importe quelle de ces vulnérabilités. Une vulnérabilité de lecture
hors limites existe dans <code>Nef_2/PM_io_parser.h</code>
<code>PM_io_parser&lt;PMDEC&gt;::read_face()</code> <code>set_halfedge()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28608">CVE-2020-28608</a>

<p>Plusieurs vulnérabilités d’exécution de code existent dans la fonction Nef
d’analyse de polygone de CGAL. Un fichier spécialement contrefait peut conduire à
une lecture hors limites et une confusion de type, ce qui pourrait conduire à
une exécution de code. Un attaquant peut fournir une entrée malveillante pour
déclencher n’importe quelle de ces vulnérabilités. Une vulnérabilité de lecture
hors limites existe dans <code>Nef_2/PM_io_parser.h</code>
<code>PM_io_parser&lt;PMDEC&gt;::read_face()</code> <code>store_fc()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28609">CVE-2020-28609</a>

<p>Plusieurs vulnérabilités d’exécution de code existent dans la fonction Nef
d’analyse de polygone de CGAL. Un fichier spécialement contrefait peut conduire à
une lecture hors limites et une confusion de type, ce qui pourrait conduire à
une exécution de code. Un attaquant peut fournir une entrée malveillante pour
déclencher n’importe quelle de ces vulnérabilités. Une vulnérabilité de lecture
hors limites existe dans <code>Nef_2/PM_io_parser.h</code>
<code>PM_io_parser&lt;PMDEC&gt;::read_face()</code> <code>store_iv()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28610">CVE-2020-28610</a>

<p>Plusieurs vulnérabilités d’exécution de code existent dans la fonction Nef
d’analyse de polygone de CGAL. Un fichier spécialement contrefait peut conduire à
une lecture hors limites et une confusion de type, ce qui pourrait conduire à
une exécution de code. Un attaquant peut fournir une entrée malveillante pour
déclencher n’importe quelle de ces vulnérabilités. Une vulnérabilité de lecture
hors limites existe dans <code>Nef_S2/SM_io_parser.h</code>
<code>SM_io_parser&lt;Decorator_&gt;::read_vertex()</code>
<code>set_face()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28611">CVE-2020-28611</a>

<p>Plusieurs vulnérabilités d’exécution de code existent dans la fonction Nef
d’analyse de polygone de CGAL. Un fichier spécialement contrefait peut conduire à
une lecture hors limites et une confusion de type, ce qui pourrait conduire à
une exécution de code. Un attaquant peut fournir une entrée malveillante pour
déclencher n’importe quelle de ces vulnérabilités. Une vulnérabilité de lecture
hors limites existe dans <code>Nef_S2/SM_io_parser.h</code>
<code>SM_io_parser&lt;Decorator_&gt;::read_vertex()</code>
<code>set_first_out_edge()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28612">CVE-2020-28612</a>

<p>Plusieurs vulnérabilités d’exécution de code existent dans la fonction Nef
d’analyse de polygone de CGAL. Un fichier spécialement contrefait peut conduire à
une lecture hors limites et une confusion de type, ce qui pourrait conduire à
une exécution de code. Un attaquant peut fournir une entrée malveillante pour
déclencher n’importe quelle de ces vulnérabilités. Une vulnérabilité de lecture
hors limites existe dans <code>Nef_S2/SNC_io_parser.h</code>
<code>SNC_io_parser&lt;EW&gt;::read_vertex()</code>
<code>vh-&gt;svertices_begin()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28613">CVE-2020-28613</a>

<p>Plusieurs vulnérabilités d’exécution de code existent dans la fonction Nef
d’analyse de polygone de CGAL. Un fichier spécialement contrefait peut conduire à
une lecture hors limites et une confusion de type, ce qui pourrait conduire à
une exécution de code. Un attaquant peut fournir une entrée malveillante pour
déclencher n’importe quelle de ces vulnérabilités. Une vulnérabilité de lecture
hors limites existe dans <code>Nef_S2/SNC_io_parser.h</code>
<code>SNC_io_parser&lt;EW&gt;::read_vertex()</code>
<code>vh-&gt;svertices_last()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28614">CVE-2020-28614</a>

<p>Plusieurs vulnérabilités d’exécution de code existent dans la fonction Nef
d’analyse de polygone de CGAL. Un fichier spécialement contrefait peut conduire à
une lecture hors limites et une confusion de type, ce qui pourrait conduire à
une exécution de code. Un attaquant peut fournir une entrée malveillante pour
déclencher n’importe quelle de ces vulnérabilités. Une vulnérabilité de lecture
hors limites existe dans <code>Nef_S2/SNC_io_parser.h</code>
<code>SNC_io_parser&lt;EW&gt;::read_vertex()</code>
<code>vh-&gt;shalfedges_begin()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28615">CVE-2020-28615</a>

<p>Plusieurs vulnérabilités d’exécution de code existent dans la fonction Nef
d’analyse de polygone de CGAL. Un fichier spécialement contrefait peut conduire à
une lecture hors limites et une confusion de type, ce qui pourrait conduire à
une exécution de code. Un attaquant peut fournir une entrée malveillante pour
déclencher n’importe quelle de ces vulnérabilités. Une vulnérabilité de lecture
hors limites existe dans <code>Nef_S2/SNC_io_parser.h</code>
<code>SNC_io_parser&lt;EW&gt;::read_vertex()</code>
<code>vh-&gt;shalfedges_last()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28616">CVE-2020-28616</a>

<p>Plusieurs vulnérabilités d’exécution de code existent dans la fonction Nef
d’analyse de polygone de CGAL. Un fichier spécialement contrefait peut conduire à
une lecture hors limites et une confusion de type, ce qui pourrait conduire à
une exécution de code. Un attaquant peut fournir une entrée malveillante pour
déclencher n’importe quelle de ces vulnérabilités. Une vulnérabilité de lecture
hors limites existe dans <code>Nef_S2/SNC_io_parser.h</code>
<code>SNC_io_parser&lt;EW&gt;::read_vertex()</code>
<code>vh-&gt;sfaces_begin()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28617">CVE-2020-28617</a>

<p>Plusieurs vulnérabilités d’exécution de code existent dans la fonction Nef
d’analyse de polygone de CGAL. Un fichier spécialement contrefait peut conduire à
une lecture hors limites et une confusion de type, ce qui pourrait conduire à
une exécution de code. Un attaquant peut fournir une entrée malveillante pour
déclencher n’importe quelle de ces vulnérabilités. Une vulnérabilité de lecture
hors limites existe dans <code>Nef_S2/SNC_io_parser.h</code>
<code>SNC_io_parser&lt;EW&gt;::read_vertex()</code>
<code>vh-&gt;sfaces_last()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28618">CVE-2020-28618</a>

<p>Plusieurs vulnérabilités d’exécution de code existent dans la fonction Nef
d’analyse de polygone de CGAL. Un fichier spécialement contrefait peut conduire à
une lecture hors limites et une confusion de type, ce qui pourrait conduire à
une exécution de code. Un attaquant peut fournir une entrée malveillante pour
déclencher n’importe quelle de ces vulnérabilités. Une vulnérabilité de lecture
hors limites existe dans <code>Nef_S2/SNC_io_parser.h</code>
<code>SNC_io_parser&lt;EW&gt;::read_vertex()</code>
<code>vh-&gt;shalfloop()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28619">CVE-2020-28619</a>

<p>Plusieurs vulnérabilités d’exécution de code existent dans la fonction Nef
d’analyse de polygone de CGAL. Un fichier spécialement contrefait peut conduire à
une lecture hors limites et une confusion de type, ce qui pourrait conduire à
une exécution de code. Un attaquant peut fournir une entrée malveillante pour
déclencher n’importe quelle de ces vulnérabilités. Une vulnérabilité de lecture
hors limites existe dans <code>Nef_S2/SNC_io_parser.h</code>
<code>SNC_io_parser&lt;EW&gt;::read_edge()</code> <code>eh-&gt;twin()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28620">CVE-2020-28620</a>

<p>Plusieurs vulnérabilités d’exécution de code existent dans la fonction Nef
d’analyse de polygone de CGAL. Un fichier spécialement contrefait peut conduire à
une lecture hors limites et une confusion de type, ce qui pourrait conduire à
une exécution de code. Un attaquant peut fournir une entrée malveillante pour
déclencher n’importe quelle de ces vulnérabilités. Une vulnérabilité de lecture
hors limites existe dans <code>Nef_S2/SNC_io_parser.h</code>
<code>SNC_io_parser&lt;EW&gt;::read_edge()</code>
<code>eh-&gt;center_vertex()</code>:.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28621">CVE-2020-28621</a>

<p>Plusieurs vulnérabilités d’exécution de code existent dans la fonction Nef
d’analyse de polygone de CGAL. Un fichier spécialement contrefait peut conduire à
une lecture hors limites et une confusion de type, ce qui pourrait conduire à
une exécution de code. Un attaquant peut fournir une entrée malveillante pour
déclencher n’importe quelle de ces vulnérabilités. Une vulnérabilité de lecture
hors limites existe dans <code>Nef_S2/SNC_io_parser.h</code>
<code>SNC_io_parser&lt;EW&gt;::read_edge()</code>
<code>eh-&gt;out_sedge()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28622">CVE-2020-28622</a>

<p>Plusieurs vulnérabilités d’exécution de code existent dans la fonction Nef
d’analyse de polygone de CGAL. Un fichier spécialement contrefait peut conduire à
une lecture hors limites et une confusion de type, ce qui pourrait conduire à
une exécution de code. Un attaquant peut fournir une entrée malveillante pour
déclencher n’importe quelle de ces vulnérabilités. Une vulnérabilité de lecture
hors limites existe dans <code>Nef_S2/SNC_io_parser.h</code>
<code>SNC_io_parser&lt;EW&gt;::read_edge()</code>
<code>eh-&gt;incident_sface()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28623">CVE-2020-28623</a>

<p>Plusieurs vulnérabilités d’exécution de code existent dans la fonction Nef
d’analyse de polygone de CGAL. Un fichier spécialement contrefait peut conduire à
une lecture hors limites et une confusion de type, ce qui pourrait conduire à
une exécution de code. Un attaquant peut fournir une entrée malveillante pour
déclencher n’importe quelle de ces vulnérabilités. Une vulnérabilité de lecture
hors limites existe dans <code>Nef_S2/SNC_io_parser.h</code>
<code>SNC_io_parser&lt;EW&gt;::read_facet()</code> <code>fh-&gt;twin()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28624">CVE-2020-28624</a>

<p>Plusieurs vulnérabilités d’exécution de code existent dans la fonction Nef
d’analyse de polygone de CGAL. Un fichier spécialement contrefait peut conduire à
une lecture hors limites et une confusion de type, ce qui pourrait conduire à
une exécution de code. Un attaquant peut fournir une entrée malveillante pour
déclencher n’importe quelle de ces vulnérabilités. Une vulnérabilité de lecture
hors limites existe dans <code>Nef_S2/SNC_io_parser.h</code>
<code>SNC_io_parser&lt;EW&gt;::read_facet()</code>
<code>fh-&gt;boundary_entry_objects</code> <code>SEdge_of</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28625">CVE-2020-28625</a>

<p>Plusieurs vulnérabilités d’exécution de code existent dans la fonction Nef
d’analyse de polygone de CGAL. Un fichier spécialement contrefait peut conduire à
une lecture hors limites et une confusion de type, ce qui pourrait conduire à
une exécution de code. Un attaquant peut fournir une entrée malveillante pour
déclencher n’importe quelle de ces vulnérabilités. Une vulnérabilité de lecture
hors limites existe dans <code>Nef_S2/SNC_io_parser.h</code>
<code>SNC_io_parser&lt;EW&gt;::read_facet()</code>
<code>fh-&gt;boundary_entry_objects</code> <code>SLoop_of</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28626">CVE-2020-28626</a>

<p>Plusieurs vulnérabilités d’exécution de code existent dans la fonction Nef
d’analyse de polygone de CGAL. Un fichier spécialement contrefait peut conduire à
une lecture hors limites et une confusion de type, ce qui pourrait conduire à
une exécution de code. Un attaquant peut fournir une entrée malveillante pour
déclencher n’importe quelle de ces vulnérabilités. Une vulnérabilité de lecture
hors limites existe dans <code>Nef_S2/SNC_io_parser.h</code>
<code>SNC_io_parser&lt;EW&gt;::read_facet()</code>
<code>fh-&gt;incident_volume()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28627">CVE-2020-28627</a>

<p>Plusieurs vulnérabilités d’exécution de code existent dans la fonction Nef
d’analyse de polygone de CGAL. Un fichier spécialement contrefait peut conduire à
une lecture hors limites et une confusion de type, ce qui pourrait conduire à
une exécution de code. Un attaquant peut fournir une entrée malveillante pour
déclencher n’importe quelle de ces vulnérabilités. Une vulnérabilité de lecture
hors limites existe dans <code>Nef_S2/SNC_io_parser.h</code>
<code>SNC_io_parser&lt;EW&gt;::read_volume()</code>
<code>ch-&gt;shell_entry_objects()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28628">CVE-2020-28628</a>

<p>Plusieurs vulnérabilités d’exécution de code existent dans la fonction Nef
d’analyse de polygone de CGAL. Un fichier spécialement contrefait peut conduire à
une lecture hors limites et une confusion de type, ce qui pourrait conduire à
une exécution de code. Un attaquant peut fournir une entrée malveillante pour
déclencher n’importe quelle de ces vulnérabilités. Une vulnérabilité de lecture
hors limites existe dans <code>Nef_S2/SNC_io_parser.h</code>
<code>SNC_io_parser&lt;EW&gt;::read_volume()</code> <code>seh-&gt;twin()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28629">CVE-2020-28629</a>

<p>Plusieurs vulnérabilités d’exécution de code existent dans la fonction Nef
d’analyse de polygone de CGAL. Un fichier spécialement contrefait peut conduire à
une lecture hors limites et une confusion de type, ce qui pourrait conduire à
une exécution de code. Un attaquant peut fournir une entrée malveillante pour
déclencher n’importe quelle de ces vulnérabilités. Une vulnérabilité de lecture
hors limites existe dans <code>Nef_S2/SNC_io_parser.h</code>
<code>SNC_io_parser&lt;EW&gt;::read_sedge()</code> <code>seh-&gt;sprev()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28630">CVE-2020-28630</a>

<p>Plusieurs vulnérabilités d’exécution de code existent dans la fonction Nef
d’analyse de polygone de CGAL. Un fichier spécialement contrefait peut conduire à
une lecture hors limites et une confusion de type, ce qui pourrait conduire à
une exécution de code. Un attaquant peut fournir une entrée malveillante pour
déclencher n’importe quelle de ces vulnérabilités. Une vulnérabilité de lecture
hors limites existe dans <code>Nef_S2/SNC_io_parser.h</code>
<code>SNC_io_parser&lt;EW&gt;::read_sedge()</code> <code>seh-&gt;snext()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28631">CVE-2020-28631</a>

<p>Plusieurs vulnérabilités d’exécution de code existent dans la fonction Nef
d’analyse de polygone de CGAL. Un fichier spécialement contrefait peut conduire à
une lecture hors limites et une confusion de type, ce qui pourrait conduire à
une exécution de code. Un attaquant peut fournir une entrée malveillante pour
déclencher n’importe quelle de ces vulnérabilités. Une vulnérabilité de lecture
hors limites existe dans <code>Nef_S2/SNC_io_parser.h</code>
<code>SNC_io_parser&lt;EW&gt;::read_sedge()</code> <code>seh-&gt;source()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28632">CVE-2020-28632</a>

<p>Plusieurs vulnérabilités d’exécution de code existent dans la fonction Nef
d’analyse de polygone de CGAL. Un fichier spécialement contrefait peut conduire à
une lecture hors limites et une confusion de type, ce qui pourrait conduire à
une exécution de code. Un attaquant peut fournir une entrée malveillante pour
déclencher n’importe quelle de ces vulnérabilités. Une vulnérabilité de lecture
hors limites existe dans <code>Nef_S2/SNC_io_parser.h</code>
<code>SNC_io_parser&lt;EW&gt;::read_sedge()</code>
<code>seh-&gt;incident_sface()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28633">CVE-2020-28633</a>


<p>Plusieurs vulnérabilités d’exécution de code existent dans la fonction Nef
d’analyse de polygone de CGAL. Un fichier spécialement contrefait peut conduire à
une lecture hors limites et une confusion de type, ce qui pourrait conduire à
une exécution de code. Un attaquant peut fournir une entrée malveillante pour
déclencher n’importe quelle de ces vulnérabilités. Une vulnérabilité de lecture
hors limites existe dans <code>Nef_S2/SNC_io_parser.h</code>
<code>SNC_io_parser&lt;EW&gt;::read_sedge()</code>
<code>seh-&gt;incident_sface()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28634">CVE-2020-28634</a>

<p>Plusieurs vulnérabilités d’exécution de code existent dans la fonction Nef
d’analyse de polygone de CGAL. Un fichier spécialement contrefait peut conduire à
une lecture hors limites et une confusion de type, ce qui pourrait conduire à
une exécution de code. Un attaquant peut fournir une entrée malveillante pour
déclencher n’importe quelle de ces vulnérabilités. Une vulnérabilité de lecture
hors limites existe dans <code>Nef_S2/SNC_io_parser.h</code>
<code>SNC_io_parser&lt;EW&gt;::read_sedge()</code> <code>seh-&gt;next()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28635">CVE-2020-28635</a>

<p>Plusieurs vulnérabilités d’exécution de code existent dans la fonction Nef
d’analyse de polygone de CGAL. Un fichier spécialement contrefait peut conduire à
une lecture hors limites et une confusion de type, ce qui pourrait conduire à
une exécution de code. Un attaquant peut fournir une entrée malveillante pour
déclencher n’importe quelle de ces vulnérabilités. Une vulnérabilité de lecture
hors limites existe dans <code>Nef_S2/SNC_io_parser.h</code>
<code>SNC_io_parser&lt;EW&gt;::read_sedge()</code> <code>seh-&gt;facet()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28636">CVE-2020-28636</a>

<p>Une vulnérabilité d’exécution de code existe dans la fonction Nef d’analyse
de polygone de CGAL. Une vulnérabilité de lecture hors limites existe dans
<code>Nef_S2/SNC_io_parser.h</code> <code>SNC_io_parser::read_sloop()</code>
<code>slh-&gt;twin()</code>. Un attaquant pouvant fournir une entrée malveillante
peut déclencher cette vulnérabilité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35628">CVE-2020-35628</a>

<p>Une vulnérabilité d’exécution de code existe dans la fonction Nef d’analyse
de polygone de CGAL. Une vulnérabilité de lecture hors limites existe dans
<code>Nef_S2/SNC_io_parser.h</code> <code>SNC_io_parser::read_sloop()</code>
<code>slh-&gt;incident_sface</code>. Un attaquant pouvant fournir une entrée
malveillante peut déclencher cette vulnérabilité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35629">CVE-2020-35629</a>

<p>Plusieurs vulnérabilités d’exécution de code existent dans la fonction Nef
d’analyse de polygone de CGAL. Un fichier spécialement contrefait peut conduire à
une lecture hors limites et une confusion de type, ce qui pourrait conduire à
une exécution de code. Un attaquant peut fournir une entrée malveillante pour
déclencher n’importe quelle de ces vulnérabilités. Une vulnérabilité de lecture
hors limites existe dans <code>Nef_S2/SNC_io_parser.h</code>
<code>SNC_io_parser&lt;EW&gt;::read_sloop()</code> <code>slh-&gt;facet()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35630">CVE-2020-35630</a>

<p>Plusieurs vulnérabilités d’exécution de code existent dans la fonction Nef
d’analyse de polygone de CGAL. Un fichier spécialement contrefait peut conduire à
une lecture hors limites et une confusion de type, ce qui pourrait conduire à
une exécution de code. Un attaquant peut fournir une entrée malveillante pour
déclencher n’importe quelle de ces vulnérabilités. Une vulnérabilité de lecture
hors limites existe dans <code>Nef_S2/SNC_io_parser.h</code>
<code>SNC_io_parser&lt;EW&gt;::read_sface()</code>
<code>sfh-&gt;center_vertex()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35631">CVE-2020-35631</a>

<p>Plusieurs vulnérabilités d’exécution de code existent dans la fonction Nef
d’analyse de polygone de CGAL. Un fichier spécialement contrefait peut conduire à
une lecture hors limites et une confusion de type, ce qui pourrait conduire à
une exécution de code. Un attaquant peut fournir une entrée malveillante pour
déclencher n’importe quelle de ces vulnérabilités. Une vulnérabilité de lecture
hors limites existe dans <code>Nef_S2/SNC_io_parser.h</code>
<code>SNC_io_parser&lt;EW&gt;::read_sface()</code>
<code>SD.link_as_face_cycle()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35632">CVE-2020-35632</a>

<p>Plusieurs vulnérabilités d’exécution de code existent dans la fonction Nef
d’analyse de polygone de CGAL. Un fichier spécialement contrefait peut conduire à
une lecture hors limites et une confusion de type, ce qui pourrait conduire à
une exécution de code. Un attaquant peut fournir une entrée malveillante pour
déclencher n’importe quelle de ces vulnérabilités. Une vulnérabilité de lecture
hors limites existe dans <code>Nef_S2/SNC_io_parser.h</code>
<code>SNC_io_parser&lt;EW&gt;::read_sface()</code>
<code>sfh-&gt;boundary_entry_objects</code> <code>Edge_of</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35633">CVE-2020-35633</a>

<p>Une vulnérabilité d’exécution de code existe dans la fonction Nef d’analyse
de polygone de CGAL. Une vulnérabilité de lecture hors limites existe dans
<code>Nef_S2/SNC_io_parser.h</code> <code>SNC_io_parser&lt;EW&gt;::read_sface()</code>
<code>store_sm_boundary_item()</code> <code>Edge_of</code>. Un fichier
spécialement contrefait peut conduire à une lecture hors limites et une
confusion de type, ce qui pourrait conduire à une exécution de code. Un attaquant
pouvant fournir une entrée malveillante pour déclencher cette vulnérabilité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35634">CVE-2020-35634</a>

<p>Une vulnérabilité d’exécution de code existe dans la fonction Nef d’analyse
de polygone de CGAL. Une vulnérabilité de lecture hors limites existe dans
<code>Nef_S2/SNC_io_parser.h</code> <code>SNC_io_parser&lt;EW&gt;::read_sface()</code>
<code>sfh-&gt;boundary_entry_objects</code> <code>Sloop_of</code>. Un fichier
spécialement contrefait peut conduire à une lecture hors limites et une
confusion de type, ce qui pourrait conduire à une exécution de code. Un attaquant
pouvant fournir une entrée malveillante pour déclencher cette vulnérabilité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35635">CVE-2020-35635</a>

<p>Une vulnérabilité d’exécution de code existe dans la fonction Nef d’analyse
de polygone de libcgal CGAL-5.1.1 de CGAL dans <code>Nef_S2/SNC_io_parser.h</code>
<code>SNC_io_parser::read_sface()</code> <code>store_sm_boundary_item()</code>
<code>Sloop_of</code>. Un fichier spécialement contrefait peut conduire à une
lecture hors limites et une confusion de type, ce qui pourrait conduire à une
exécution de code. Un attaquant pouvant fournir une entrée malveillante pour
déclencher cette vulnérabilité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-35636">CVE-2020-35636</a>

<p>Une vulnérabilité d’exécution de code existe dans la fonction Nef d’analyse
de polygone de libcgal CGAL-5.1.1 de CGAL dans <code>Nef_S2/SNC_io_parser.h</code>
<code>SNC_io_parser::read_sface()</code> <code>sfh-&gt;volume()</code>. Un
fichier spécialement contrefait peut conduire à une lecture hors limites et une
confusion de type, ce qui pourrait conduire à une exécution de code. Un
attaquant pouvant fournir une entrée malveillante pour déclencher cette
vulnérabilité.</p></li>

</ul>

<p>Pour Debian 10 « Buster », ces problèmes ont été corrigés dans
la version 4.13-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets cgal.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de cgal,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/cgal">\
https://security-tracker.debian.org/tracker/cgal</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3226.data"
# $Id: $
