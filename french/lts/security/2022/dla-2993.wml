#use wml::debian::translation-check translation="9d6086988fc18ca3ea5068a4c529b8367f1a8e64" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un problème de sécurité a été découvert dans la bibliothèque de
compression libz-mingw-w64.</p>

<p>Danilo Ramos a découvert qu'un traitement incorrect de la mémoire dans
la gestion de « deflate » dans libz-mingw-w64 pouvait avoir pour
conséquences un déni de service ou éventuellement l'exécution de code
arbitraire lors du traitement d'une entrée contrefaite pour l'occasion.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
1.2.11+dfsg-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libz-mingw-w64.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libz-mingw-w64,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libz-mingw-w64">\
https://security-tracker.debian.org/tracker/libz-mingw-w64</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2993.data"
# $Id: $
