#use wml::debian::translation-check translation="76f7d48bf1cc50a87d4b3de7d43e58c3a0eaf92f" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un problème a été découvert dans Cinder d’OpenStack, un service de stockage
en mode bloc. En fournissant une image simple VMDK spécialement créée qui
référençait un chemin spécifique de fichier de sauvegarde, un utilisateur
authentifié pouvait persuader des systèmes de renvoyer une copie du contenu de
fichier par le serveur, aboutissant à un accès non autorisé à des données
éventuellement sensibles.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 2:13.0.7-1+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets cinder.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de cinder,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/cinder">\
https://security-tracker.debian.org/tracker/cinder</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3301.data"
# $Id: $
