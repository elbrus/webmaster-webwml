#use wml::debian::translation-check translation="4242b316e8a2081b456403eb6b3ae009c97efbf0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux bogues importants ont été découverts dans xapian-core, une bibliothèque
de moteur de recherche, qui pourraient conduire à une corruption possible de
base de données par disque plein et un rapport incorrect de la corruption pour
une base de données avec réplication d’ensembles de modifications.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1.4.11-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets xapian-core.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de xapian-core,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/xapian-core">\
https://security-tracker.debian.org/tracker/xapian-core</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3355.data"
# $Id: $
