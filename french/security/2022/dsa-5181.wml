#use wml::debian::translation-check translation="3f23b56e7451102ddf68b78669a3f961f3d25e97" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Request Tracker, un
système extensible de gestion de tickets de problèmes.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-25802">CVE-2022-25802</a>

<p>Request Tracker est vulnérable à une attaque par script intersite (XSS)
lors de l'affichage du contenu de pièces jointes avec des types de contenus
frauduleux.</p></li>

</ul>

<p>Il a été découvert en plus que Request Tracker ne réalisait pas une
vérification complète des droits lors d'accès à des champs personnalisés de
type fichier ou image, permettant éventuellement l'accès à ces champs
personnalisés à des utilisateurs sans droit d'accès aux objets associés,
avec pour conséquence la divulgation d'informations.</p>

<p>Pour la distribution oldstable (Buster), ces problèmes ont été corrigés
dans la version 4.4.3-2+deb10u2.</p>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 4.4.4+dfsg-2+deb11u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets request-tracker4.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de request-tracker4,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/request-tracker4">\
https://security-tracker.debian.org/tracker/request-tracker4</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5181.data"
# $Id: $
