#use wml::debian::translation-check translation="0e15219c67c419f6ccdb0f041ab3fedd1ea44795" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans l'hyperviseur Xen qui
pouvaient avoir pour conséquence une élévation de privilèges. En complément,
cette mise à jour fournit des atténuations pour l'attaque d'exécution
spéculative <q>Retbleed</q> et les vulnérabilités <q>MMIO stale data</q>.</p>

<p>Pour davantage d'informations, veuillez consulter les pages suivantes :
<a href="https://xenbits.xen.org/xsa/advisory-404.html">https://xenbits.xen.org/xsa/advisory-404.html</a>
<a href="https://xenbits.xen.org/xsa/advisory-407.html">https://xenbits.xen.org/xsa/advisory-407.html</a></p>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 4.14.5+24-g87d90d511c-1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets xen.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de xen, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/xen">\
https://security-tracker.debian.org/tracker/xen</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5184.data"
# $Id: $
