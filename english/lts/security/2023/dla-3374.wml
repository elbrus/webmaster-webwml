<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in linmicrohttpd, a library embedding HTTP server
functionality. Parsing crafted POST requests result in an out of bounds
read, which might cause a DoS (Denial of Service).</p>


<p>For Debian 10 buster, this problem has been fixed in version
0.9.62-1+deb10u1.</p>

<p>We recommend that you upgrade your libmicrohttpd packages.</p>

<p>For the detailed security status of libmicrohttpd please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libmicrohttpd">https://security-tracker.debian.org/tracker/libmicrohttpd</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3374.data"
# $Id: $
