<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential denial-of-service (DoS) attack
in the Kamailio SIP telephony server. This was caused by the Kamailio server
mishandling <code>INVITE</code> requests with duplicated fields.</p>
<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27507">CVE-2020-27507</a>

    <p>The Kamailio SIP before 5.5.0 server mishandles INVITE requests with duplicated fields and overlength tag, leading to a buffer overflow that crashes the server or possibly have unspecified other impact.</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, this problem has been fixed in version
5.2.1-1+deb10u1.</p>

<p>We recommend that you upgrade your kamailio packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3438.data"
# $Id: $
