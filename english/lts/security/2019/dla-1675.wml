<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Alexander Kjäll and Stig Palmquist discovered a vulnerability in
python-gnupg, a wrapper around GNU Privacy Guard. It was possible to
inject data through the passphrase property of the gnupg.GPG.encrypt()
and gnupg.GPG.decrypt() functions when symmetric encryption is used.
The supplied passphrase is not validated for newlines, and the library
passes --passphrase-fd=0 to the gpg executable, which expects the
passphrase on the first line of stdin, and the ciphertext to be
decrypted or plaintext to be encrypted on subsequent lines.</p>

<p>By supplying a passphrase containing a newline an attacker can
control/modify the ciphertext/plaintext being decrypted/encrypted.</p>


<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
0.3.6-1+deb8u1.</p>

<p>We recommend that you upgrade your python-gnupg packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1675.data"
# $Id: $
