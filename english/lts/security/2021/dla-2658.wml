<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several issues were found in Redmine, a project management web application,
which could lead to cross-site scripting, information disclosure, and reading
arbitrary files from the server.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
3.3.1-4+deb9u4.</p>

<p>We recommend that you upgrade your redmine packages.</p>

<p>For the detailed security status of redmine please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/redmine">https://security-tracker.debian.org/tracker/redmine</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2658.data"
# $Id: $
