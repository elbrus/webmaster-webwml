<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several memory safety issues affecting the RPC protocol were fixed in
p11-kit, a library providing a way to load and enumerate PKCS#11
modules.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29361">CVE-2020-29361</a>

    <p>Multiple integer overflows</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-29362">CVE-2020-29362</a>

    <p>Heap-based buffer over-read</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
0.23.3-2+deb9u1.</p>

<p>We recommend that you upgrade your p11-kit packages.</p>

<p>For the detailed security status of p11-kit please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/p11-kit">https://security-tracker.debian.org/tracker/p11-kit</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2513.data"
# $Id: $
