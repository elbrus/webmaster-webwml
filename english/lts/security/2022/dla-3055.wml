<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in NTFS-3G, a read-write NTFS
driver for FUSE. A local user can take advantage of these flaws for
local root privilege escalation.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-30783">CVE-2022-30783</a>

    <p>An invalid return code in fuse_kern_mount enables intercepting of
    libfuse-lite protocol traffic between NTFS-3G and the kernel when
    using libfuse-lite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-30784">CVE-2022-30784</a>

    <p>A crafted NTFS image can cause heap exhaustion in
    ntfs_get_attribute_value.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-30785">CVE-2022-30785</a>

    <p>A file handle created in fuse_lib_opendir, and later used in
    fuse_lib_readdir, enables arbitrary memory read and write
    operations when using libfuse-lite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-30786">CVE-2022-30786</a>

    <p>A crafted NTFS image can cause a heap-based buffer overflow in
    ntfs_names_full_collate.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-30787">CVE-2022-30787</a>

    <p>An integer underflow in fuse_lib_readdir enables arbitrary memory
    read operations when using libfuse-lite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-30788">CVE-2022-30788</a>

    <p>A crafted NTFS image can cause a heap-based buffer overflow in
    ntfs_mft_rec_alloc.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-30789">CVE-2022-30789</a>

     <p>A crafted NTFS image can cause a heap-based buffer overflow in
     ntfs_check_log_client_array.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1:2016.2.22AR.1+dfsg-1+deb9u3.</p>

<p>We recommend that you upgrade your ntfs-3g packages.</p>

<p>For the detailed security status of ntfs-3g please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ntfs-3g">https://security-tracker.debian.org/tracker/ntfs-3g</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3055.data"
# $Id: $
