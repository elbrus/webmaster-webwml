<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in Samba, a SMB/CIFS file,
print, and login server for Unix.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1472">CVE-2020-1472</a>

    <p>Unauthenticated domain controller compromise by subverting Netlogon
    cryptography.  This vulnerability includes both ZeroLogon and
    non-ZeroLogon variations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10704">CVE-2020-10704</a>

    <p>An unauthorized user can trigger a denial of service via a stack
    overflow in the AD DC LDAP server.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10730">CVE-2020-10730</a>

    <p>NULL pointer de-reference and use-after-free in Samba AD DC LDAP
    Server with ASQ, VLV and paged_results.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10745">CVE-2020-10745</a>

    <p>Denial of service resulting from abuse of compression of replies to
    NetBIOS over TCP/IP name resolution and DNS packets causing excessive
    CPU load on the Samba AD DC.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10760">CVE-2020-10760</a>

    <p>The use of the paged_results or VLV controls against the Global
    Catalog LDAP server on the AD DC will cause a use-after-free.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14303">CVE-2020-14303</a>

    <p>Denial of service resulting from CPU spin and and inability to
    process further requests once the AD DC NBT server receives an empty
    (zero-length) UDP packet to port 137.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14318">CVE-2020-14318</a>

    <p>Missing handle permissions check in ChangeNotify</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14323">CVE-2020-14323</a>

    <p>Unprivileged user can crash winbind via invalid lookupsids DoS</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14383">CVE-2020-14383</a>

    <p>DNS server crash via invalid records resulting from uninitialized
    variables</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
2:4.5.16+dfsg-1+deb9u3.</p>

<p>We recommend that you upgrade your samba packages.</p>

<p>For the detailed security status of samba please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/samba">https://security-tracker.debian.org/tracker/samba</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2463.data"
# $Id: $
