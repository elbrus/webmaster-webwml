<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A regression has been found in the patch for <a href="https://security-tracker.debian.org/tracker/CVE-2016-10711">CVE-2016-10711</a> of pound, a
reverse proxy, load balancer and HTTPS front-end for Web servers.
Without the fix pound can be tricked to use 100% CPU.</p>


<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2.6-6+deb8u3.</p>

<p>We recommend that you upgrade your pound packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2196-2.data"
# $Id: $
