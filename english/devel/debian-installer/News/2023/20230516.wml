<define-tag pagetitle>Debian Installer Bookworm RC 3 release</define-tag>
<define-tag release_date>2023-05-16</define-tag>
#use wml::debian::news

<p>
The Debian Installer <a
href="https://wiki.debian.org/DebianInstaller/Team">team</a> is pleased to
announce the third release candidate of the installer for Debian 12
<q>Bookworm</q>.
</p>


<h2>Improvements in this release</h2>

<ul>
  <li>finish-install:
  <ul>
    <li>Adjust APT cache cleaning to avoid breaking bash completion
    (<a href="https://bugs.debian.org/1034650">#1034650</a>).</li>
  </ul>
  </li>
  <li>grub-installer:
  <ul>
    <li>Detect EFI boot variables with hexadecimal digits, not only
    decimal digits.</li>
  </ul>
  </li>
  <li>hw-detect:
  <ul>
    <li>Restore support for firmware license prompts (<a href="https://bugs.debian.org/1033921">#1033921</a>).</li>
  </ul>
  </li>
  <li>linux:
  <ul>
    <li>Build against updated dwarves, reducing its size and memory
    footprint (<a href="https://bugs.debian.org/1033301">#1033301</a>).</li>
  </ul>
  </li>
  <li>partman-base:
  <ul>
    <li>Add support for input submitted using power-of-two units: kiB,
    MiB, GiB, etc. (<a href="https://bugs.debian.org/913431">#913431</a>). Note that sizes are still output using
    power-of-ten units: kB, MB, GB, etc.</li>
    <li>Add support for bigger prefixes: petabyte (PB), pebibyte (PiB),
    exabyte (EB), and exbibyte (EiB).</li>
    <li>With many thanks to Vincent Danjean!</li>
  </ul>
  </li>
  <li>preseed:
  <ul>
    <li>Make sure netcfg considers DHCP-provided hostnames, only using
    the hostname parameter on the kernel command line as a fallback
    (<a href="https://bugs.debian.org/1035349">#1035349</a>).</li>
  </ul>
  </li>
</ul>


<h2>Hardware support changes</h2>

<ul>
  <li>debian-installer:
  <ul>
    <li>Ship dedicated DRM modules for bochs and cirrus to avoid broken
    graphics under UEFI/Secure Boot (<a href="https://bugs.debian.org/1036019">#1036019</a>).</li>
  </ul>
  </li>
  <li>linux:
  <ul>
    <li>Work around black screen on ppc64el (<a href="https://bugs.debian.org/1033058">#1033058</a>).</li>
  </ul>
  </li>
  <li>xorg-server:
  <ul>
    <li>Ship modesetting_drv.so in the udeb again, fixing graphical
    installer support on UTM (<a href="https://bugs.debian.org/1035014">#1035014</a>).</li>
  </ul>
  </li>
</ul>


<h2>Localization status</h2>

<ul>
  <li>78 languages are supported in this release.</li>
  <li>Full translation for 41 of them.</li>
</ul>


<h2>Known issues in this release</h2>

<p>
See the <a href="$(DEVEL)/debian-installer/errata">errata</a> for
details and a full list of known issues.
</p>


<h2>Feedback for this release</h2>

<p>
We need your help to find bugs and further improve the installer, so please
try it. Installer CDs, other media and everything else you will need are
available at our <a href="$(DEVEL)/debian-installer">web site</a>.
</p>


<h2>Thanks</h2>

<p>
The Debian Installer team thanks everybody who has contributed to this
release.
</p>
