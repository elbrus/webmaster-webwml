#use wml::debian::translation-check translation="b35f3578f60fe18b9150a0924ba18826c4d6be4c"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Ken Gaillot descubrió una vulnerabilidad en el gestor de recursos
en cluster Pacemaker: si se configuraban ACL para usuarios pertenecientes al grupo
<q>haclient</q>, las restricciones impuestas por las ACL podrían sortearse mediante comunicación IPC
no restringida, dando lugar a ejecución de código arbitrario en el cluster con
privilegios de root.</p>

<p>Si no está habilitada la opción del cluster <q>enable-acl</q>, los miembros del
grupo <q>haclient</q> pueden modificar la configuración («Cluster Information Base») de Pacemaker sin
restricciones, lo que ya les da las capacidades descritas en el párrafo anterior, por lo que
no hay una exposición adicional en este caso.</p>

<p>Para la distribución «estable» (buster), este problema se ha corregido en
la versión 2.0.1-5+deb10u1.</p>

<p>Le recomendamos que actualice los paquetes de pacemaker.</p>

<p>Para información detallada sobre el estado de seguridad de pacemaker, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/pacemaker">\
https://security-tracker.debian.org/tracker/pacemaker</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4791.data"
