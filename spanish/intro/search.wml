#use wml::debian::template title="Cómo usar el motor de búsqueda de Debian" MAINPAGE="true"
#use wml::debian::translation-check translation="6686348617abaf4b5672d3ef6eaab60d594cf86e"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<p>
El proyecto Debian ofrece su propio motor de búsqueda: <a
href="https://search.debian.org/">https://search.debian.org/</a>.
Estos son algunos consejos sobre cómo usarlo y cómo realizar tanto búsquedas sencillas como
otras más complejas utilizando operadores booleanos.
</p>

<div class="row">
  <!-- left column -->
  <div class="column column-left">
    <div style="text-align: left">
      <h3>Búsqueda simple</h3>
      <p>La forma más simple de usar el motor es escribir una sola palabra en el campo de búsqueda y pulsar «intro» o hacer click en el botón <em>Búsqueda</em>. El motor de búsqueda devolverá todas las páginas de nuestro sitio web que contengan esa palabra. Esto debería proporcionarle buenos resultados en la mayoría de las búsquedas.</p>
      <p>Otra opción es buscar más de una palabra. De esta forma, debería obtener todas las páginas del sitio web de Debian que contengan todas las palabras introducidas. Para buscar frases, escríbalas entre comillas ("). Tenga en cuenta que el motor de búsqueda no distingue entre mayúsculas y minúsculas, por lo que si, por ejemplo, busca <code>gcc</code> obtendrá las páginas que contengan «gcc» y también las que contengan «GCC».</p> 
      <p>Bajo el campo de búsqueda tiene dos desplegables que le permiten seleccionar el número de resultados mostrados por página y el idioma. La búsqueda en el sitio web de Debian soporta casi 40 idiomas diferentes.</p>
    </div>
  </div>

<!-- right column -->
  <div class="column column-right">
    <div style="text-align: left">
      <h3>Búsqueda booleana</h3>
      <p>Si la búsqueda simple no es suficiente, puede usar operadores booleanos. Puede elegir entre <em>AND</em>, <em>OR</em> y <em>NOT</em>, y también puede combinarlos. Asegúrese de escribir los operadores en mayúsculas para que el motor de búsqueda los reconozca.</p>

      <ul>
        <li><b>AND</b> combina dos expresiones y devuelve páginas que contienen ambas. Por ejemplo, <code>gcc AND patch</code> encuentra todas las páginas que contienen ambas palabras: «gcc» y «patch». En este caso concreto, obtendrá el mismo resultado que si busca <code>gcc patch</code>, pero un <code>AND</code> explícito puede ser útil en combinación con otros operadores.</li>
        <li><b>OR</b> devuelve páginas que contienen cualquiera de las palabras. <code>gcc OR patch</code> encuentra las páginas que contienen «gcc» o «patch» (o ambas).</li>
        <li><b>NOT</b> se usa para excluir términos de búsqueda de los resultados. Por ejemplo, <code>gcc NOT patch</code> encuentra todas las páginas que contienen «gcc» pero que no contienen «patch». <code>gcc AND NOT patch</code> da el mismo resultado, pero buscar <code>NOT patch</code> no está soportado.</li>
        <li><b>(...)</b> se puede usar para agrupar expresiones. Por ejemplo, <code>(gcc OR make) NOT patch</code> encontrará todas las páginas que contengan «gcc» o «make» (o ambas) pero que no contengan «patch».</li>
      </ul>
    </div>
  </div>
</div>
