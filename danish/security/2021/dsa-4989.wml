#use wml::debian::translation-check translation="21199125ec27fcc04d1d656a9f0bf8050249091a" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Efterforskere hos United States of America National Security Agency (NSA) 
opdagede to lammelsesangrebssårbarheder i strongSwan, en IKE/IPsec-suite.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-41990">CVE-2021-41990</a>

    <p>RSASSA-PSS-signaturer hvis parametre definerer en meget høj salt-længde, 
    kunne udløse et heltalsoverløb, som kunne føre til en segmenteringsfejl.</p>

    <p>Generering af en signatur, der omgår padding-tjekket til at udløse 
    nedbrudet, kræver adgang til den private nøgle, som signerede certifikatet. 
    Dog behøver der ikke at være tillid til certifikatet.  Da plugin'erne gmp og 
    openssl begge tjekker om et fortolket certifikat er selvsigneret (og 
    signaturen er gyldig), kan det for eksempel udløses af et ikke-relateret 
    selvsigneret CA-certifikat, sendt af en initiator.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-41991">CVE-2021-41991</a>

    <p>Når certifikatcachen i hukommelsen er fyldt op, prøver den på tilfældig 
    vis at erstatte mindre benyttede forekomster.  Afhængigt af den genererede 
    tilfældige værdi, kunne det føre til et heltalsoverløb, som medfører en 
    double-dereference og et kald, som benytter hukommelse udenfor grænserne, 
    der sandsynligvis vil føre til en segmenteringsfejl.</p>

    <p>Fjernudførelse af kode kan ikke helt udelukkes, men angribere har ingen 
    kontrol over den dereferencerede hukommelse, så det virker usandsynligt på 
    nuværende tidspunkt.</p></li>

</ul>

<p>I den gamle stabile distribution (buster), er disse problemer rettet
i version 5.7.2-1+deb10u1.</p>

<p>I den stabile distribution (bullseye), er disse problemer rettet i
version 5.9.1-1+deb11u1.</p>

<p>Vi anbefaler at du opgraderer dine strongswan-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende strongswan, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/strongswan">\
https://security-tracker.debian.org/tracker/strongswan</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4989.data"
