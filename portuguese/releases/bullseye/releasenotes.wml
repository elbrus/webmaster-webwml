#use wml::debian::template title="Debian 11 -- Notas de lançamento" BARETITLE=true
#use wml::debian::release
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/arches.data"
#include "$(ENGLISHDIR)/releases/bullseye/release.data"
#use wml::debian::translation-check translation="ce7e85638a047b969574a7abeb8a634c27b9086b"

<if-stable-release release="stretch">
<p>Esta é uma <strong>versão em construção</strong> das notas de lançamento
do Debian 10, codinome buster, que ainda não foi lançado. A informação
apresentada aqui pode estar imprecisa ou desatualizada, e provavelmente está
incompleta.</p>
</if-stable-release>

<if-stable-release release="buster">
<p>Esta é uma <strong>versão em construção</strong> das notas de lançamento
do Debian 11, codinome bullseye, que ainda não foi lançado. A informação
apresentada aqui pode estar imprecisa ou desatualizada, e provavelmente está
incompleta.</p>
</if-stable-release>

<p>Para saber mais sobre as novidades do Debian 11, veja as notas de lançamento
para sua arquitetura:</p>

<ul>
<:= &permute_as_list('release-notes/', 'Release Notes'); :>
</ul>

<p>As notas de lançamento também contêm instruções para usuários(as) que estejam
atualizando de versões anteriores.</p>

<p>Se você definiu a localização do seu navegador de forma
apropriada, você pode usar o link acima para obter a versão HTML correta
automaticamente &mdash; veja sobre
<a href="$(HOME)/intro/cn">negociação de conteúdo</a>.
De outra forma, escolha a arquitetura exata, idioma e formato que você deseja
da tabela abaixo.</p>

<div class="centerdiv">
<table class="reltable">
<tr>
  <th align="left"><strong>Arquitetura</strong></th>
  <th align="left"><strong>Formato</strong></th>
  <th align="left"><strong>Idiomas</strong></th>
</tr>
<: &permute_as_matrix_new( file => 'release-notes', langs => \%langsrelnotes,
                           formats => \%formats, arches => \@arches,
                           html_file => 'release-notes/index' ); :>
</table>
</div>
