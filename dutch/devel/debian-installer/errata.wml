#use wml::debian::template title="Het installatiesysteem van Debian - errata"
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"
#use wml::debian::translation-check translation="9a1725c7a7c2a16470f0814224c0b78cecb2e2fe"

<h1>Errata voor <humanversion /></h1>
q
<p>
Dit is een lijst met gekende problemen in de <humanversion />-release van het
installatiesysteem van Debian. Indien u uw probleem hierin niet vermeld
vindt, stuur ons dan een
<a href="$(HOME)/releases/stable/amd64/ch05s04#submit-bug">installatierapport</a>
waarin u het probleem beschrijft.
</p>

<dl class="gloss">
     <dt>In het installatiesysteem gebruikt thema</dt>
     <dd>Er bestaat nog geen grafische vormgeving voor Bookworm, en het
     installatiesysteem gebruikt nog altijd het grafisch thema van Bullseye
     <br />
     <b>Status:</b> Volledig opgelost in Bookworm RC 1.</dd>

     <dt> Voor sommige geluidskaarten is firmware vereist</dt>
     <dd> Er schijnen een aantal geluidskaarten te zijn die firmware moeten
     laden om geluid te kunnen produceren
     (<a href="https://bugs.debian.org/992699">#992699</a>).
     <br />
     <b>Status:</b> Opgelost in Bookworm Alpha 1.</dd>

     <dt>Versleuteld LVM kan mislukken op systemen met weinig geheugen</dt>
     <dd>Het is mogelijk dat systemen met weinig geheugen (bv. 1 GB) er niet in
     slagen versleuteld LVM op te zetten: cryptsetup kan het out-of-memory
     killerproces uitlokken bij het formatteren van de LUKS-partitie.
     (<a href="https://bugs.debian.org/1028250">#1028250</a>,
     <a href="https://gitlab.com/cryptsetup/cryptsetup/-/issues/802">bovenstroomse ontwikkelaars van cryptsetup</a>).
     <br />
     <b>Status:</b> Opgelost in Bookworm RC 2.
     </dd>

     <dt>In UEFI-modus werkt de schijfindeling “Begeleid - volledige schijf
     gebruiken en LVM instellen” niet correct</dt>
     <dd>Het gebruik van dit soort begeleide schijfindeling resulteert in de
     prompt "UEFI-installatie afdwingen?" (zelfs als er geen andere
     besturingssystemen zijn), waarbij het standaardantwoord ingesteld staat
     op "Nee"
     (<a href="https://bugs.debian.org/1033913">#1033913</a>).
     Vasthouden aan het standaardantwoord zal zeer waarschijnlijk resulteren in
     een niet-opstartende installatie als Secure Boot is ingeschakeld.
     Overschakelen naar "Ja" zou moeten voorkomen dat u dit probleem tegenkomt.
     <br />
     <b>Status:</b> Opgelost in Bookworm RC 2.
     </dd>

</dl>
