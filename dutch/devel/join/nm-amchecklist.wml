#use wml::debian::template title="Checklist voor kandidatuurbeheerders"
#use wml::debian::translation-check translation="5011f532637dc7820b79b151eecfda4ab65aa22f"

<p>
<b>Opmerking:</b> De wikipagina<a href="https://wiki.debian.org/FrontDesk/AMTutorial">AM Tutorial</a> is actueler dan deze pagina.
</p>

<p>Deze checklist heeft alleen betrekking op de belangrijkste onderdelen van de NM-controles. Afhankelijk van de achtergrond en de plannen in het project van de <a href="./newmaint#Applicant">kandidaat</a>, kan een <a href="./newmaint#AppMan">kandidatuurbeheerder</a> ervoor kiezen sommige van de hier genoemde zaken te negeren of andere toe te voegen.</p>

<p>Zie ook de <a href="nm-amhowto">Mini-HOWTO voor kandidatuurbeheerders</a>.</p>

<h3><a name="identification">Identificatiecontrole</a></h3>
<p>De <a href="./newmaint#Applicant">kandidaat</a> moet een publieke
OpenPGP-sleutel hebben die minstens door één <a href="./newmaint#Member">\
lid van Debian</a> werd ondertekend. Indien mogelijk wordt ook ten minste één andere handtekening van een OpenPGP-sleutel met goede interconnecties vereist. Gebruik steeds <tt>gpg --check-sigs</tt> en niet <tt>gpg --list-sigs</tt> om de identiteit van een kandidaat te verifiëren.</p>

<p>De OpenPGP-sleutel die naar de Debian-sleutelbos gaat, moet een versie 4-sleutel zijn. Om dit te controleren, neemt u de vingerafdruk van de sleutel en controleert u of deze 32 of 40 hexadecimale cijfers lang is. Versie 3-sleutels hebben slechts 32 cijfers, bij versie 4 zijn dat 40 cijfers. Deze sleutel hoeft niet dezelfde te zijn als de sleutel die wordt gebruikt om de identiteit van de kandidaat te verifiëren.</p>

<p>Kandidaten <em>moeten</em> een encryptiesleutel hebben. Controleer dit door
<tt>gpg --list-keys <var>&lt;SleutelID&gt;</var></tt> uit te voeren.
Als de uitvoer geen regel bevat met
<tt><var>&lt;Getal&gt;</var>E/<var>&lt;SleutelID&gt;</var></tt> of
<tt><var>&lt;Getal&gt;</var>g/<var>&lt;SleutelID&gt;</var></tt>, moet de
kandidaat een encryptiesubsleutel toevoegen.</p>

<p>Als de <a href="./newmaint#Applicant">kandidaat</a> geen ondertekende sleutel kan overleggen, kan een door de overheid uitgegeven identiteitsbewijs met foto worden gebruikt voor identificatie. Neem in dergelijke gevallen contact op met de <a href="./newmaint#FrontDesk">frontdesk</a>.</p>

<p>Bij twijfel over de identiteit van de kandidaat kan gebruik worden gemaakt van aanvullende verificatiemogelijkheden:</p>

<ul>
 <li>Als de kandidaat een student is, kan iemand van de universiteit zijn identiteit bevestigen. Deze persoon moet ook vermeld staan op de webpagina's van het universiteitspersoneel.</li>

 <li>Als de kandidaat in een groter bedrijf werkt, zou zijn werkgever zijn identiteit moeten kunnen bevestigen.</li>

 <li>Er zijn websites voor het opzoeken van telefoonnummers die de mogelijkheid bieden om omgekeerd te zoeken, hoewel dit normaal gesproken niet werkt voor mobiele telefoons. Het nummer dat de kandidaat opgeeft moet ofwel overeenkomen met zijn eigen naam of de persoon die de telefoon beantwoordt moet de identiteit van de kandidaat kunnen bevestigen.</li>
</ul>

<h3><a name="pandp">Filosofie en procedures</a></h3>
<p>Er zijn geen vaste regels voor dit onderdeel, maar sommige gebieden moeten altijd aan bod komen (en het verdient aanbeveling de andere te bespreken):</p>
<ul>
 <li>Kandidaten moeten ermee instemmen de <a
  href="$(DOC)/debian-policy/">Debian beleidsrichtlijnen</a> en het <a
  href="$(DEVEL)/dmup">Beleid van Debian inzake machinegebruik</a> (Debian Machine Usage Policy - DMUP) te handhaven.</li>

 <li>Kandidaten moeten akkoord gaan met het <a href="$(HOME)/social_contract">Sociaal contract</a> en moeten kunnen uitleggen hoe Debian zich verhoudt tot de Vrije Softwaregemeenschap.</li>

 <li>Kandidaten moeten een goed begrip hebben van de <a href="$(HOME)/social_contract#guidelines">Debian-richtlijnen voor Vrije Software</a> (Debian Free Software Guidelines). Ze moeten kunnen bepalen of een licentie vrij is of niet en ze moeten een uitgesproken mening hebben over Vrije Software.</li>

 <li>Kandidaten moeten begrijpen hoe het Debian Bug Tracking Systeem, het Bugvolgsysteem van Debian, werkt, welke informatie Debian daarin bewaart (pseudo-pakketten, wnpp, ...) en hoe ze het kunnen hanteren.</li>

 <li>Kandidaten moeten op de hoogte zijn van de Debian QA-processen (verwezen, verwijderen, NMU-en en QA-uploads).</li>

 <li>Kandidaten moeten het release-proces van Debian begrijpen.</li>

 <li>Kandidaten moeten de l10n- en i18n-inspanningen van Debian kennen en weten wat ze kunnen doen om deze te helpen.</li>
</ul>

<h3><a name="tands">Taken en Vaardigheden</a></h3>
<p>Wat moet vallen onder het controleren van T&amp;V, hangt af van het gebied waarin de kandidaat werkzaam wil zijn:</p>

<ul>
 <li>Kandidaten die willen werken als verpakker <em>moeten</em> ten minste één pakket in het archief hebben. Het pakket moet voldoende gebruikers hebben om een basis te bieden voor het aantonen van de verpakkingsvaardigheden van de kandidaat en zijn manier van omgaan met gebruikers, indieners van bugs en bugs.
  <br />
  Verdere vragen moeten ook betrekking hebben op enkele basisaspecten van het verpakkingswerk voor Debian (configuratiebestanden, menu's, init-scripts, deelaspecten van het beleid, het geschikt maken voor andere architecturen, complexe afhankelijkheden).</li>

 <li>Kandidaten die van plan zijn documentatie te schrijven, moeten al voorbeelden van hun werk hebben gegeven. Zij moeten ook een duidelijke visie hebben over het soort documenten waaraan zij in de toekomst willen werken.</li>
</ul>

<h3><a name="finalreport">Eindrapport van de kandidatuurbeheerder aan de Debian accountbeheerder</a></h3>
<p>Nadat alle controles zijn afgerond en de kandidatuurbeheerder tevreden is met de prestaties van de kandidaat, moet een verslag worden ingediend bij de Debian accountbeheerder en de frontdesk voor nieuwe leden. Het moet documenteren wat er is gedaan om te voldoen aan de verschillende onderdelen van de controles voor nieuwe leden en het moet ook alle verzamelde informatie over de kandidaat bevatten.</p>

<p>De e-mail moet gericht worden aan &lt;da-manager@debian.org&gt; en
 &lt;nm@debian.org&gt; en de volgende zaken bevatten:</p>

<ul>
 <li>Een kort overzicht van de kandidatuur, met wat basisinformatie over de kandidaat.</li>

 <li>De accountnaam die de kandidaat heeft aangevraagd. Deze moet minimaal 3 tekens lang zijn.</li>

 <li>Het e-mailadres waarnaar alle e-mail gericht aan <var>&lt;account&gt;</var>@debian.org moet worden doorgestuurd.</li>

 <li>De vingerafdruk van de openbare OpenPGP-sleutel van de kandidaat die moet worden opgenomen in de sleutelbos van Debian.</li>

 <li>Een met gzip gecomprimeerde mbox met logs van alle conversatie tussen de kandidatuurbeheerder en de kandidaat over de kandidatuur.</li>
</ul>

<p>Hiermee zijn de verantwoordelijkheden van de kandidatuurbeheerder in het kandidaatstellingsproces afgerond. De frontdesk voor nieuwe leden en de accountbeheerder zullen het rapport over de kandidatuur controleren en beoordelen.</p>

<hr />
<a href="newmaint">Terug naar de hoek voor nieuwe leden</a>
